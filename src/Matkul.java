
import java.util.*;

public class Matkul {
    private String kode;
    private String namaMatkul;
    private double nilaiAngka;
    private static List<Matkul> MatkulList;

    public static List<Matkul> getMatkulList() {
        return MatkulList;
    }

    public String getKode() {
        return kode;
    }

    public String getNamaMatkul() {
        return namaMatkul;
    }

    public double getNilaiAngka() {
        return nilaiAngka;
    }

    public void tambahMatkul(Matkul Matkul) {
        MatkulList.add(Matkul);
    }

    Matkul(String kode, String namaMatkul, double nilaiAngka){
        this.kode = kode;
        this.namaMatkul = namaMatkul;
        this.nilaiAngka = nilaiAngka;
        Matkul.MatkulList = new ArrayList<>();
    }

    public String getNilai(){
        if (nilaiAngka >= 80 && nilaiAngka <= 100) {
            return "A";
        } else if (nilaiAngka >= 70 && nilaiAngka < 80) {
            return "B";
        } else if (nilaiAngka >= 60 && nilaiAngka < 70) {
            return "C";
        } else if (nilaiAngka >= 50 && nilaiAngka < 60) {
            return "D";
        } else if (nilaiAngka >= 0 && nilaiAngka < 50) {
            return "E";
        } else {
            return "Nilai tidak valid!";
        }

    }

   
}
