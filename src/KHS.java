import java.util.*;

public class KHS {
    private Mahasiswa mahasiswa;
    private static List<KHS> khsList = new ArrayList<>();

    public static void tambahKHS(KHS khs) {
        khsList.add(khs);
    }

    public static List<KHS> getKHSList() {
        return khsList;
    }
  
    public KHS(Mahasiswa mahasiswa) {
        this.mahasiswa = mahasiswa;
    }
    
    void cetakKHS(List<Matkul> matkulListKHS) {
        System.out.println("\n========================= Kartu Hasil Studi =========================");
        System.out.printf("|%-66s |\n","Nama   : " + mahasiswa.getNama());
        System.out.printf("|%-66s |\n","NIM    : " + mahasiswa.getNIM());
        System.out.println("---------------------------------------------------------------------");
        System.out.printf("| %-10s | %-20s | %-15s | %-11s |\n", "Kode MK", "Nama MK", "Nilai Angka", "Nilai");
        System.out.println("---------------------------------------------------------------------");
        for (Matkul mk : matkulListKHS) {
            System.out.printf("| %-10s | %-20s | %-15.2f | %-11s |\n", mk.getKode(), mk.getNamaMatkul(), mk.getNilaiAngka(),
                    mk.getNilai());
        }
        System.out.println("---------------------------------------------------------------------");
    }

}
