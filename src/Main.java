import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);  
        System.out.println("=== Aplikasi Perekaman KHS ===");

        boolean tambahMahasiswa = true;
        while (tambahMahasiswa) {
            System.out.print("\nMasukkan Nama Mahasiswa: ");
            String nama = sc.nextLine();
            System.out.print("Masukkan NIM Mahasiswa: ");
            String nim = sc.nextLine();
            Mahasiswa mahasiswa = new Mahasiswa(nama, nim);
            Mahasiswa.tambahMahasiswa(mahasiswa);

            System.out.println("\nData KHS untuk Mahasiswa dengan Nama: " + mahasiswa.getNama());
            KHS khs = new KHS(mahasiswa);
            boolean tambahMatkul = true;
            while (tambahMatkul) {
                System.out.println("\nMasukkan data mata kuliah:");
                System.out.print("Kode MK: ");
                String kode = sc.nextLine();
                System.out.print("Nama MK: ");
                String namaMK = sc.nextLine();
                System.out.print("Nilai Angka: ");
                double nilaiAngka = sc.nextDouble();
                sc.nextLine();

                Matkul Matkul = new Matkul(kode, namaMK, nilaiAngka);
                Matkul.tambahMatkul(Matkul);

                System.out.print("\nApakah Anda ingin menambah data mata kuliah lagi? (y/n): ");
                String pilihanMatkul = sc.nextLine();
                tambahMatkul = (pilihanMatkul.equalsIgnoreCase("y"));
            }
            KHS.tambahKHS(khs);

            System.out.print("\nApakah Anda ingin menambah mahasiswa lagi? (y/n): ");
            String pilihanMahasiswa = sc.nextLine();
            tambahMahasiswa = (pilihanMahasiswa.equalsIgnoreCase("y"));
        }

        System.out.println("\n=== Kartu Hasil Studi (KHS) ===");
        
        for (KHS khs : KHS.getKHSList()) {
            List<Matkul> matkulListKHS = Matkul.getMatkulList();
            khs.cetakKHS(matkulListKHS);
            System.out.println();
        }

        sc.close();
    }
}

