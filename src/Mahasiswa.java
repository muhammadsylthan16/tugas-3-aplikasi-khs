import java.util.*;

public class Mahasiswa {
    private String nama;
    private String NIM;
    private static List<Mahasiswa> mahasiswaList = new ArrayList<>();
    
    public String getNama() {
        return nama;
    }

    public String getNIM() {
        return NIM;
    }

    Mahasiswa(String nama, String NIM ){
        this.nama = nama;
        this.NIM = NIM;
        
    }

    public static void tambahMahasiswa(Mahasiswa mahasiswa) {
        mahasiswaList.add(mahasiswa);
    }

    public static List<Mahasiswa> getMahasiswaList() {
        return mahasiswaList;
    }
}
